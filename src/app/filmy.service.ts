import { Injectable } from '@angular/core';
import { Film } from './modele/film'


@Injectable({
  providedIn: 'root'
})
export class FilmyService {

  private filmy: Film[] = [
    {id:0, tytul:'Bottle Rocket', rok: 1996, opis: 'Trzech nieudaczników zakłada gang złodziei i obmyśla pierwszy napad. Nie potrafią jednak sprostać najprostszym zadaniom, co dopiero mówić o poważnym skoku.'},
    {id:1, tytul:'Rushmore', rok: 1998, opis: 'Do bardzo elitarnej akademii Rushmore uczęszcza Max Fischer, jest on dość zdolnym uczniem, na każdym kroku pokazuje swoje zainteresowanie przedmiotem, aczkolwiek grozi mu wydalenie ze szkoły. Jednak prawdziwe kłopoty dopiero przed nim, bowiem zakochuje się w nauczycielce klas pierwszych, pani Cross.'},
    {id:2, tytul:'The Royal Tenenbaums', rok: 2001, opis: 'Tenenbaumowie to rodzina geniuszy, od której odszedł ojciec. Mężczyzna powraca po latach, by pogodzić się z dziećmi i żoną dowiedziawszy się o swojej postępującej chorobie. Ale czy na pewno?'},
    {id:3, tytul:'The Life Aquatic with Steve Zissou', rok: 2004, opis: 'Steve Zissou jest światowej sławy oceanografem, słynnym ze swych filmów dokumentalnych opisujących podwodną faunę i florę. Jego najbliższy przyjaciel i współpracownik został pożarty przez rekina podczas ostatniej wyprawy. Zissou zaczyna organizować swą najbardziej kosztowną ekspedycję, której celem jest odnalezienie i zemsta na legendarnym rekinie. '},
    {id:4, tytul:'Hotel Chevalier', rok: 2007, opis: 'Krótki film będący prologiem do pełnometrażowego "Pociągu do Darjeeling". Jack od miesiąca mieszka w paryskim hotelu. Pewnego wieczora otrzymuje telefon od byłej dziewczyny, która oznajmia, że jedzie się z nim zobaczyć.'},
    {id:5, tytul:'The Darjeeling Limited', rok: 2007, opis: 'Trzech skłóconych ze sobą braci postanawia wyruszyć wspólnie w podróż po Indiach, by podjąć próbę odbudowania rodzinnych więzi. Ich "duchowa podróż" nie obędzie się bez niespodzianek, w których główne role odegrają tabletki przeciwbólowe, indyjski syrop na kaszel i gaz pieprzowy.'},
    {id:6, tytul:'Fantastic Mr. Fox', rok: 2009, opis: 'Pan Lis jest lisem z krwi i kości. Kiedy żona poinformowała go o swojej ciąży, obiecał jej, że zrezygnuje z obecnej pracy (zaiste lisiej, bo trudził się łapaniem kur i innego ptactwa). Znajduje sobie ciepłą posadkę w gazecie - pisze do niej teksty, których nikt nie czyta. jednak farmerzy z małego miasteczka postanawiają pozbyć się wykradającego im zbiory lisa i jego rodziny'},
    {id:7, tytul:'Moonrise Kingdom', rok: 2012, opis: 'Dwukrotnie nominowany do Oscara reżyser Wes Anderson przedstawia wzruszającą historię uczucia, które latem 1965 roku zawładnęło sercami dwojga nastolatków. Zauroczeni sobą młodzi nie zważają na surowe zakazy rodziców i opiekunów, zawierają sekretny pakt i uciekają, by razem przeżyć największą przygodę życia w bezkresnej głuszy lasów Nowej Anglii.'},
    {id:8, tytul:'The Grand Budapest Hotel', rok: 2014, opis: 'Niezwykłe przygody ekscentrycznego portiera oraz jego lobby-boya ze słynnego europejskiego hotelu w burzliwym okresie międzywojennym, który uwikłany zostaje w aferę wokół kradzieży bezcennego renesansowego obrazu i walkę o przejęcie ogromnej fortuny rodzinnej.'},
    {id:9, tytul:'Isle of Dogs', rok: 2018, opis: 'Opowieść o dwunastoletnim japońskim chłopcu imieniem Atari, którego wychowuje skorumpowany burmistrz wielkiego miasta Megasaki. Gdy na mocy specjalnego dekretu wszystkie psy domowe wygnane zostają na ogromne wysypisko odpadów, Atari leci na miniaturowej machinie powietrznej na „wyspę śmieci”, by odnaleźć swego ukochanego psa.'},
  ];

  constructor() { }

  wszystkieFilmy(): Film[]  {
    return this.filmy;
  }
  getFilm(id: number): Film  {
    return this.filmy[id];
  }
}
